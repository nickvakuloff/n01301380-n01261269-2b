﻿<%@ Control Language="C#" AutoEventWireup="true" ViewStateMode="Enabled" CodeBehind="CodeBox.ascx.cs" Inherits="Guide.usercontrols.CodeBox" %>
    <asp:DataGrid ID="Code" runat="server" CssClass="code"
        GridLines="None" Width="100%" CellPadding="2" >
        <HeaderStyle Font-Size="Large" BackColor="#999966" ForeColor="White" />
        <ItemStyle BackColor="#1a1d23" ForeColor="#ffffff"/>     
    </asp:DataGrid>
