﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Guide.usercontrols
{
    public partial class CodeBox : System.Web.UI.UserControl
    {
        public string CodeToServe
        {
            get { return (string)ViewState["CodeToServe"]; }
            set { ViewState["CodeToServe"] = value; }
        }
        DataView CreateCodeSource()
        {
            var page = (Page)HttpContext.Current.CurrentHandler;
            DataTable codedata = new DataTable();


            //First column is the line number of the code (idx -> index)
            DataColumn idx_col = new DataColumn();

            //SEcond column is the actual code itself
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            idx_col.ColumnName = "Line";
            idx_col.DataType = System.Type.GetType("System.Int32");

            codedata.Columns.Add(idx_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);

            List<string> mycode;

            if(CodeToServe == "wApplicationCode")
            {
                mycode = new List<string>(new string[]{
                "~/*First of all I write main properties of the my future menu.*/",
                ".mobile-menu{",
                "~position: fixed;",
                "~top: 67px;",
                "~left: 0;",
                "~height: 100%;",
                "~width: 100%;",
                "~background-color: #000;",
                "~opacity: 0.9;",
                "~z-index: 100;",
                "~display: none;  /*Here I hide the menu, because I dont need it in desktop version of the website.*/",
                "}",
                ".mobile-menu-active{  /*I register a second state of my menu, when It appears.*/",
                "~display: block;",
                "}",
                " ",
                " ",
                "@media only screen and(max-width: 992px) {  /*Then I hide desktop menu using media. */",
                "~#main-menu{",
                "~~display: none;",
                "~}",
                " ",
                "$('.hamburger').click(function () {  /*Here I say when hamburger will clicked add to the element with class 'mobile-menu' add class 'mobile-menu-active'*/ ",
                "$('.mobile-menu').toggle('.mobile-menu-active');  /*If element with id 'mobile-menu' already has 'mobile-menu-active' delete it*/ ",
                "})"
                });
            } else if (CodeToServe=="wProgrammingCode")
            {
                mycode = new List<string>(new string[]{
                "var book1 = 'Lord of the rings',",
                "var book2 = 'Sherlock Holmes'",
                "var book3 = 'Shannara chronicles'",
                "var book4 = 'Bartimeus trilogy'",
                "var book5 = 'Warcraft chronicles'",
                "var book6 = 'Harry Potter'",
                "var book7 = 'Piter Pan'",
                "var book8 = 'War and Piece'",
                "var book9 = 'Crime and Punishment'",
                "var book10 = 'The Witcher'",
                "var books = [' '];",
                "var l = 0;",
                "for (var i = 1; i<11;    i++){",
                "~books.push(eval('book' + i));",
                "~console.log('Book' + i + ': ' + books[i]);",
                "}"
                });

            }
            else
            {
                mycode = new List<string>(new string[]{
                "--Select data from the field vendor_name and combine data from the list of fields through the coma and space",
                "SELECT vendor_name, vendor_city ',' || vendor_state || ' ' || vendor_zip_code AS address",
                "--Fields relate to the vendors table",
                "FROM vendors",
                "--Sort by field address, if we got same address make a second sort by the vendor_name",
                "ORDER BY address, vendor_name"
                });
            }     

            //These Tildas will be replaced with line indents
            //When the code lines are bound to rows.
            //If you have double quotes in your html code, escap them with \
            //eg. <div style=\"background:green;\"></div>

            int i = 0;
            foreach (string code_line in mycode)
            {
                coderow = codedata.NewRow();
                coderow[idx_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                codedata.Rows.Add(coderow);
            }


            DataView codeview = new DataView(codedata);
            return codeview;
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            this.Page.Master.EnableViewState = true;
            DataView ds = CreateCodeSource();
            Code.DataSource = ds;

            /*Some formatting in the codebehind*/

            Code.DataBind();
        }

    }
}