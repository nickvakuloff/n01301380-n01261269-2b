﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="contact_form.ascx.cs" Inherits="Guide.usercontrols.contact_form" %>
<div id="form">
    <button type="button" class="form-button">Callback form</button>
<div class="callback-form">
            <h1 id="info" runat="server">Callback form</h1>
            <asp:Label Text="Enter your name:" runat="server" />
                <br />
            <asp:TextBox runat="server" ID="client_name" placeholder="Enter your name"/>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Name" ControlToValidate="client_name" ID="validatorName"></asp:RequiredFieldValidator>
            <br />
            <asp:Label Text="Enter your phone:" runat="server" />
                <br />
            <asp:TextBox runat="server" ID="client_phone" placeholder="Enter your phone number"/>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a phone number" ControlToValidate="client_phone" ID="validatorPhoneNumber"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter valid Phone number" ControlToValidate="client_phone" ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$" ></asp:RegularExpressionValidator>
                <br />
                <asp:Label Text="Enter your email:" runat="server" />
                <br />
            <asp:TextBox runat="server" ID="client_email" placeholder="Enter your email"/>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter email" ControlToValidate="client_email" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
            <!--stolen from https://stackoverflow.com/questions/18585613/how-do-you-validate-phone-number-in-asp-net -->
            <br />
            <asp:Label ID="thx" runat="server" />
            <br />
            <asp:Button runat="server" ID="myButton" Text="Submit" OnClick="Submit_Form"/>
        </div>
</div>
