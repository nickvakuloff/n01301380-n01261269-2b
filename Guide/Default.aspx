﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Guide._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="hidden">Home</h1>
    <h2 class="content-title">Choose the course</h2>
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="course-wrapper">
                <a href="/WApplication.aspx">
                    <img class="img-responsive" src="img/wa1.png" alt="C# pic" />
                </a>
                <p class="course-description">
                    In this hands-on course students will integrate all of web 
                    programming skills they have learned in previous semesters to 
                    build a complex, database-driven web application. Their final 
                    product must be standards-compliant and be accessible from mobile 
                    devices of varying form factors, desktop computers and home 
                    entertainment systems.
                </p>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="course-wrapper">
                <a class="course" href="/WDevelopment.aspx">
                    <img class="img-responsive" src="img/js1.png" alt="Javascript pic" />
                </a>
                <p class="course-description">
                    Students are introduced to website development using HTML and Cascading 
                    Style Sheets. Using JavaScript, students will gain an introduction to 
                    event-driven programing that will allow more dynamic content to be included 
                    within websites. Students will further explore web development with AJAX and 
                    learn new technologies such as HTML5 and JQuery.
                </p>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="course-wrapper">
                <a class="course" href="/Database.aspx">
                    <img class="img-responsive" src="img/db1.png" alt="Database pic" />
                </a>
                <p class="course-description">
                    This course is designed to introduce students to database design and 
                    development, based on Structured Query Language (SQL) and two industry-standard 
                    databases: MySQL and Oracle. The student will learn how to access the database 
                    to retrieve data from tables and how to apply transactions to database tables. 
                    Students will also create stored procedures on the database.
                </p>
            </div>
        </div>
    </div>
</asp:Content>

<%--<asp:Content ContentPlaceHolderID="Form" runat="server">
    <uctrl:contact runat="server" id="contact_form" />
</asp:Content>--%>
